import datetime
import json


FIVE_MINUTES = datetime.timedelta(seconds = 300)


def parse_datetime(s):
	assert s.endswith('Z')
	if '.' in s:
		s = s.split('.', 1)[0] + '+00:00'
	else:
		s = s[:-1] + '+00:00'
	return datetime.datetime.fromisoformat(s)


async def process(request):
	now = datetime.datetime.now(datetime.timezone.utc)
	obj = json.loads(await request.text())
	assert obj['version'] == '1'
	alerts = []
	for a in obj['alerts']:
		startTime = parse_datetime(a['startsAt'])
		endTime = parse_datetime(a['endsAt'])
		if now - startTime < FIVE_MINUTES or now - endTime < FIVE_MINUTES:
			alerts.append(f'{a["labels"]["alertname"]} {a["status"]}')
	if alerts:
		return f'[{obj["status"]}] {", ".join(alerts)}'
	else:
		return f'[{obj["status"]}]'
